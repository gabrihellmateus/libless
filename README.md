# Lib LESS

>  A collection of LESS mixins, classes and functions.

## Content

 - [grid.less](https://github.com/gabrihellmateus/libless/blob/master/grid.less)
 - [mixins.less](https://github.com/gabrihellmateus/libless/blob/master/mixins.less)
 - [print.less](https://github.com/gabrihellmateus/libless/blob/master/print.less)
 - [wordpress.less](https://github.com/gabrihellmateus/libless/blob/master/wordpress.less)

## Install with Bower
You can install this collecion with [bower](http://bower.io/).

*"Bower depends on [Node](http://nodejs.org/) and [npm](https://www.npmjs.org/). It's installed globally using npm"*

```
npm install -g bower
```

Then install `libless`:

```
bower install libless
```

## License
Copyright (c) 2014 gabrihellmateus

Licensed under the [MIT license](https://github.com/gabrihellmateus/libless/blob/master/LICENSE.txt).